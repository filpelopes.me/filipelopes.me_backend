<?php

require("config-db.php");

header('Access-Control-Allow-Origin: *');  
header('Content-Type: application/json');  

$serverip = $_SERVER["REMOTE_ADDR"];

function ipRepetido($ip, $table){
    global $mydb;

    $query = $mydb->query("SELECT * FROM $table WHERE ip = '$ip' ")or die("Não foi possível pesquisa ip");
    $num = $query->num_rows;
    return ($num > 0 ? true : false);

}

function getView($table){
    global $mydb;

    $query = $mydb->query("SELECT * FROM $table")or die("Não foi encontrada a tabela");
    $num1 = $query->num_rows;
    
    return $num1;
}

function addView($table){
    global $mydb;
    global $serverip;

    if(ipRepetido($serverip, $table) == false){
        $datetime = date('Y-m-d H:i:s');
        $query = $mydb->query("INSERT INTO `$table` (`id`, `ip`, `datetime`) VALUES (NULL, '$serverip', '$datetime')");
        
        return true;
    }

    return false;
}

if(addView("views_site")){
    echo "{ 
        \"views\" : ".getView("views_site")."
    }";
}else{
    echo "{
        \"views\" : ".getView("views_site").", 
        \"message\" : \"IP já acessado\"
    }";
}



?>