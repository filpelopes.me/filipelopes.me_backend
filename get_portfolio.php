<?php

require("config-db.php");

header('Access-Control-Allow-Origin: *');  
header('Content-Type: application/json');

function splitString($str){
    $array = explode(",", $str);
    foreach($array as $item){
        $item = trim($item);
    }
}

class Item {
    public function __construct($name, $slug, $categories, $description, $tags, $items){
        $this->name = $name;
        $this->slug = $slug;

        $categories = explode("," , $categories);
        foreach ($categories as $key => $category){
            $categories[$key] = trim($category);
        }

        $this->categories = $categories;
        $this->description = $description;

        $tags = explode("," , $tags);
        foreach ($tags as $key => $tag){
            $tags[$key] = trim($tag);
        }

        $this->tags = $tags;
        $this->items = json_decode($items);
    }
}

class ItemPortfolio {
    public function __construct($type, $url){
        $this->type = $type;
        $this->url = $url;
    } 
}

function get_items(){
    global $mydb;
    $array = array();

    $query = $mydb->query("SELECT * FROM portfolio")or die("Não foi encontrada a tabela");
    if($query->num_rows){
        while($row = $query->fetch_assoc()){
            // array_push($array, json_decode($row["items"]));
            $item = new Item($row["name"], $row["slug"], $row["categories"], $row["description"], $row["tags"], $row["items"]);
            array_push($array, $item);
        }
    }

    return $array;
}

echo json_encode(get_items());


?>